package test;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import mt.util.MaxRetryCount;
import org.testng.annotations.Test;

import static org.assertj.core.api.Assertions.assertThat;

@Feature("Search")
@Story("Search Result")
public class SearchTest extends BaseTest {

    @MaxRetryCount(2)
    @Test(description = "Check search results")
    void checkSearchResults() {
        var searchKey = "Rafadan Tayfa";

        var nameList = getHomePage()
                .searchTo(searchKey)
                .getSearchResultNameList();

        nameList.forEach(s -> assertThat(s).containsIgnoringCase(searchKey));
    }
}
