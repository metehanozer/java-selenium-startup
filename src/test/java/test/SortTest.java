package test;

import io.qameta.allure.Feature;
import io.qameta.allure.Story;
import mt.util.MaxRetryCount;
import org.testng.annotations.Test;

import static mt.enums.SortType.PRICE_LOW;
import static org.assertj.core.api.Assertions.assertThat;

@Feature("Search")
@Story("Sort Search Result")
public class SortTest extends BaseTest {

    @MaxRetryCount(1)
    @Test(description = "Check low price sorting")
    void checkLowPriceSorting() {
        var searchKey = "Rafadan Tayfa";

        var priceList = getHomePage()
                .searchTo(searchKey)
                .sortResultsBy(PRICE_LOW)
                .getPriceList();

        assertThat(priceList).isSorted();
    }
}
