package test;

import io.qameta.allure.Allure;
import io.qameta.allure.Step;
import mt.page.HomePage;
import mt.util.DriverFactory;
import mt.config.ConfigManager;
import mt.util.TestListener;
import org.testng.annotations.*;

import java.time.Duration;

import static mt.config.StaticConfig.Selenium.TIMEOUT;

@Listeners(TestListener.class)
public class BaseTest {

    @BeforeTest
    @Parameters({"hub", "browser"})
    public void setUpDriverBeforeTest(@Optional("http://localhost:4444") String hub, @Optional("chrome") String browser) {
        var intDataListSample = ConfigManager.getInstance().getIntList("int.data.list");
        var strDataListSample = ConfigManager.getInstance().getStrList("str.data.list");
        System.out.println("intDataListSample: " + intDataListSample);
        System.out.println("strDataListSample: " + strDataListSample);

        DriverFactory.setupWebDriver(hub, browser);
        var driver = DriverFactory.getDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(TIMEOUT));
        Allure.step(browser + " driver thread: " + Thread.currentThread().getId());
    }

    @Step("Get home page")
    protected HomePage getHomePage() {
        var homeUrl = ConfigManager.getInstance().getStr("home.url");
        return new HomePage(DriverFactory.getDriver()).openHomePage(homeUrl);
    }

    @AfterTest
    public void tearDownDriverAfterTest() {
        Allure.step("driver thread: " + Thread.currentThread().getId());
        DriverFactory.getDriver().manage().deleteAllCookies();
        DriverFactory.tearDownWebDriver();
    }
}
