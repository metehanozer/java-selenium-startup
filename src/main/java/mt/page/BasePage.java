package mt.page;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import io.qameta.allure.Attachment;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

import static mt.config.StaticConfig.Selenium.POLLING;
import static mt.config.StaticConfig.Selenium.TIMEOUT;

public class BasePage {

    protected WebDriver driver;
    protected WebDriverWait wait;

    protected Gson gson = new GsonBuilder().setPrettyPrinting().create();

    public BasePage(WebDriver driver) {
        this.driver = driver;
        wait = new WebDriverWait(driver, Duration.ofSeconds(TIMEOUT), Duration.ofMillis(POLLING));
    }

    protected void waitUntilPageReady() {
        wait.until(d -> ((JavascriptExecutor) d).executeScript("return document.readyState").equals("complete"));
    }

    protected WebElement findElement(By by) {
        wait.until(ExpectedConditions.presenceOfElementLocated(by));
        return driver.findElement(by);
    }

    protected List<WebElement> findElements(By by) {
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(by));
        return driver.findElements(by);
    }

    public <T> void scrollIntoView(T attr) {
        WebElement element = attr.getClass().getName().contains("By") ? findElement((By) attr) : (WebElement) attr;
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
    }

    public <T> String getText(T attr) {
        WebElement element = attr.getClass().getName().contains("By") ? findElement((By) attr) : (WebElement) attr;
        wait.until(ExpectedConditions.visibilityOf(element));
        return element.getText();
    }

    public <T> List<String> getTextList(T attr) {
        List<WebElement> elementList = attr.getClass().getName().contains("By")
                ? findElements((By) attr)
                : (List<WebElement>) attr;
        List<String> textList = new ArrayList<>();
        for (WebElement element : elementList) {
            //scrollIntoView(element);
            textList.add(getText(element));
        }
        return textList;
    }

    public <T> String getAttribute(T attr, String nameAttr) {
        WebElement element = attr.getClass().getName().contains("By") ? findElement((By) attr) : (WebElement) attr;
        wait.until(ExpectedConditions.visibilityOf(element));
        return element.getAttribute(nameAttr);
    }

    public <T> void mouseOver(T attr) {
        WebElement element = attr.getClass().getName().contains("By") ? findElement((By) attr) : (WebElement) attr;
        wait.until(ExpectedConditions.visibilityOf(element));
        new Actions(driver).moveToElement(element).build().perform();
        sleepWhile(20);
    }

    public <T> void click(T attr) {
        WebElement element = attr.getClass().getName().contains("By") ? findElement((By) attr) : (WebElement) attr;
        wait.until(ExpectedConditions.elementToBeClickable(element));
        element.click();
        sleepWhile(50);
    }

    /**
     * Element Click Intercepted oluşursa biraz bekleyip 5 tekrara kadar deniyor.
     */
    public <T> void click(T attr, boolean handleInterceptedClick) {
        WebElement element = attr.getClass().getName().contains("By") ? findElement((By) attr) : (WebElement) attr;

        int loopCount = 0;
        while (handleInterceptedClick) {
            try {
                click(element);
                handleInterceptedClick = false;
            } catch (ElementClickInterceptedException ex) {
                sleepWhile(250);
                loopCount++;
                if (loopCount == 5) throw new ElementClickInterceptedException(ex.toString());
            }
        }
    }

    public <T> void javascriptClick(T attr) {
        WebElement element = attr.getClass().getName().contains("By") ? findElement((By) attr) : (WebElement) attr;
        wait.until(ExpectedConditions.visibilityOf(element));
        ((JavascriptExecutor) driver).executeScript("arguments[0].click();", element);
        sleepWhile(50);
    }

    public <T> void sendKeys(T attr, String value) {
        WebElement element = attr.getClass().getName().contains("By") ? findElement((By) attr) : (WebElement) attr;
        wait.until(ExpectedConditions.visibilityOf(element));
        element.clear();
        element.sendKeys(value);
        sleepWhile(50);
    }

    public <T> void sendKeysWithEnter(T attr, String value) {
        WebElement element = attr.getClass().getName().contains("By") ? findElement((By) attr) : (WebElement) attr;
        wait.until(ExpectedConditions.visibilityOf(element));
        element.clear();
        element.sendKeys(Keys.chord(value, Keys.ENTER));
        sleepWhile(50);
    }

    protected void customSelectByValue(WebElement customSelect, String value) {
        click(customSelect);
        var customSelectItem = By.cssSelector(String.format("[data-value='%s']", value));
        click(customSelectItem);
        wait.until(ExpectedConditions.attributeContains(customSelectItem, "class", "selected"));
        sleepWhile(50);
    }

    @Attachment(value = "{0}", type = "image/png")
    protected byte[] takeScreenshot(String title) {
        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    }

    /**
     * Senkronizasyonu daha kolay sağlamak için bazı işlemlerden sonra biraz bekleniyor.
     */
    protected void sleepWhile(int milisecond) {
        try {
            Thread.sleep(milisecond);
        } catch (InterruptedException ignored) { }
    }
}
