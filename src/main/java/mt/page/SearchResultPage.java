package mt.page;

import io.qameta.allure.Allure;
import io.qameta.allure.Step;
import mt.enums.SortType;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.ArrayList;
import java.util.List;

public class SearchResultPage extends BasePage {

    @FindBy(css = "#smartListOption .custom-select")
    WebElement customSelect;

    @FindBy(id = "listingUl")
    WebElement productListElement;

    @FindBy(css = ".productName")
    List<WebElement> productNameList;

    @FindBy(css = ".newPrice")
    List<WebElement> productPriceList;

    public SearchResultPage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
        waitUntilSearchResultsDisplayed();
    }

    @Step("wait until search results displayed")
    private void waitUntilSearchResultsDisplayed() {
        wait.until(ExpectedConditions.visibilityOf(productListElement));
        takeScreenshot("Search Result Page");
    }

    @Step("Get search result name list")
    public List<String> getSearchResultNameList() {
        List<String> nameList = getTextList(productNameList);
        Allure.addAttachment("Search result name list", gson.toJson(nameList));
        return nameList;
    }

    @Step("Sort results by")
    public SearchResultPage sortResultsBy(SortType sortType) {
        var priceList = getTextList(productPriceList);
        customSelectByValue(customSelect, sortType.get());
        waitUntilSearchResultPriceListChange(priceList);
        return this;
    }

    @Step("Get price list")
    public List<Float> getPriceList() {
        List<Float> priceList = new ArrayList<>();
        for (var productPrice : productPriceList) {
            var price = productPrice.getText().replace(" TL", "").replace(",",".");
            priceList.add(Float.parseFloat(price));
        }
        Allure.addAttachment("Price list", gson.toJson(priceList));
        return priceList;
    }

    public SearchResultPage sortByLowPrice() {
        var priceList = getTextList(productPriceList);
        customSelectByValue(customSelect, SortType.PRICE_LOW.get());
        waitUntilSearchResultPriceListChange(priceList);
        return this;
    }

    private void waitUntilSearchResultPriceListChange(List<String> priceList) {
        wait.until((ExpectedCondition<Boolean>) driver -> {
            var newPriceList = getTextList(productPriceList);
            return priceList != newPriceList;
        });
    }
}
