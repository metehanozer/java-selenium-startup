package mt.page;

import io.qameta.allure.Allure;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class HomePage extends BasePage {

    @FindBy(id = "searchData")
    WebElement searchInput;

    @FindBy(css = ".catMenu")
    WebElement catMenu;

    By acceptAllCookiesButton = By.xpath("//span[text()='Tümünü Kabul Et']");;

    public HomePage(WebDriver driver) {
        super(driver);
        PageFactory.initElements(driver, this);
    }

    @Step("Open home page")
    public HomePage openHomePage(String homeUrl) {
        driver.get(homeUrl);
        waitUntilPageLoads();
        acceptAllCookies();
        return this;
    }

    @Step("Wait until home page loads")
    public HomePage waitUntilPageLoads() {
        waitUntilPageReady();
        wait.until(driver -> catMenu.isDisplayed());
        takeScreenshot("Home Page");
        return this;
    }

    @Step("Accept all cookies if exist in 3 second")
    public HomePage acceptAllCookies() {
        try {
            new WebDriverWait(driver, Duration.ofSeconds(3))
                    .until(ExpectedConditions.visibilityOfElementLocated(acceptAllCookiesButton))
                    .click();
        } catch (TimeoutException ignored) {
            Allure.step("Cookies does't exist");
        }
        return this;
    }

    @Step("Search to {value}")
    public SearchResultPage searchTo(String value) {
        sendKeysWithEnter(searchInput, value);
        return new SearchResultPage(driver);
    }
}
