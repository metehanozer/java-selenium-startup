package mt.enums;

public enum SortType {
    PRICE_LOW("PRICE_LOW"),
    PRICE_HIGH("PRICE_HIGH");

    final String type;

    SortType(String type) {
        this.type = type;
    }

    public String get() {
        return type;
    }
}
