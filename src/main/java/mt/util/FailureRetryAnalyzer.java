package mt.util;

import org.testng.IRetryAnalyzer;
import org.testng.ITestResult;

public class FailureRetryAnalyzer implements IRetryAnalyzer {

    int currentRetry = 0;

    @Override
    public boolean retry(ITestResult result) {
        MaxRetryCount maxRetryCount = result.getMethod()
                .getConstructorOrMethod()
                .getMethod()
                .getAnnotation(MaxRetryCount.class);

        int maxRetryCountValue = maxRetryCount != null ? maxRetryCount.value() : 1;

        if (++currentRetry > maxRetryCountValue) {
            currentRetry = 0;
            return false;
        } else return true;
    }
}
