package mt.util;

import io.qameta.allure.Attachment;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class TestListener implements ITestListener {

    @Override
    public void onTestSuccess(ITestResult result) {
        System.out.printf("%s %s PASSED%n", addTestName(result), addDecorator(result));
    }

    @Override
    public void onTestSkipped(ITestResult result) {
        System.out.printf(String.format("%s %s SKIPPED%n", addTestName(result), addDecorator(result)));
    }

    @Override
    public void onTestFailure(ITestResult result) {
        System.out.printf("%s %s FAILED%n", addTestName(result), addDecorator(result));
        takeScreenShot();
    }

    @Attachment(value = "Failure Screenshot", type = "image/png")
    private byte[] takeScreenShot() {
        return ((TakesScreenshot) DriverFactory.getDriver()).getScreenshotAs(OutputType.BYTES);
    }

    private String addTestName(ITestResult result) {
        return result.getMethod().getConstructorOrMethod().getName();
    }

    private String addDecorator(ITestResult result) {
        return "=".repeat(100 - addTestName(result).length());
    }
}
