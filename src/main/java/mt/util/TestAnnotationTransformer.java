package mt.util;

import org.testng.IAnnotationTransformer;
import org.testng.annotations.ITestAnnotation;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

public class TestAnnotationTransformer implements IAnnotationTransformer {

    @Override
    public void transform(ITestAnnotation annotation, Class clas, Constructor constructor, Method method) {
        if (method == null) return;
        MaxRetryCount maxRetryCount = method.getAnnotation(MaxRetryCount.class);
        if (maxRetryCount == null) return;
        annotation.setRetryAnalyzer(FailureRetryAnalyzer.class);
    }
}
