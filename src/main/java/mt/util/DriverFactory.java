package mt.util;

import lombok.SneakyThrows;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;

import java.net.URL;
import java.util.Optional;

public class DriverFactory {

    private DriverFactory() {
    }

    private static final ThreadLocal<WebDriver> driver = new ThreadLocal<>();
    private static final String ERROR_MSG = "WebDriver instance error for thread " + Thread.currentThread().getId();

    public static WebDriver getDriver() {
        return Optional.ofNullable(driver.get()).orElseThrow(() -> new IllegalStateException(ERROR_MSG));
    }

    @SneakyThrows
    public static void setupWebDriver(String hub, String browser) {
        driver.set(createBrowserInstance(new URL(hub), browser));
    }

    public static void tearDownWebDriver() {
        Optional.ofNullable(driver.get()).orElseThrow(() -> new IllegalStateException(ERROR_MSG)).quit();
        driver.remove();
    }

    private static WebDriver createBrowserInstance(URL hub, String browser) {
        switch (browser.toLowerCase()) {

            case "chrome":
                var chromeOptions = BrowserOptions.getChromeOptions();
                return new RemoteWebDriver(hub, chromeOptions);

            case "edge":
                var edgeOptions = BrowserOptions.getEdgeOptions();
                return new RemoteWebDriver(hub, edgeOptions);

            case "firefox":
                var firefoxOptions = BrowserOptions.getFirefoxOptions();
                return new RemoteWebDriver(hub, firefoxOptions);

            default:
                throw new IllegalArgumentException(browser + " browser is not supported");
        }
    }
}
