# java-selenium-startup

Java TestNG ve Selenium Page Factory kullanılarak yazılmış UI testleri için demo proje. 
Doğrulama için AssertJ kütüphanesi kullanıldı. Html raporlama için Allure kullanıldı.

Seri test koşumu için...
~~~
mvn clean test -D env=prod -D suite=Serial -D hub=http://localhost:4444 -D browser=chrome
~~~

Paralel test koşumu için...
~~~
mvn clean test -D env=prod -D suite=Parallel -D hub=http://localhost:4444 -D browser=edge
~~~

Allure test raporu...
~~~
allure serve .\target\allure-results\
~~~

![Alt text](allure.png?raw=true)
